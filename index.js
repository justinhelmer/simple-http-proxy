var dotenv = require('dotenv'),
    fs = require('fs'),
    https = require('https'),
    httpProxy = require('http-proxy'),
    path = require('path'),
    express = require('express');

dotenv.config();

const relativePath = process.env.RELATIVE_PATH;
if (!relativePath) {
  throw new Error('process.env.RELATIVE_PATH must be the relative path to the project directory (relative to proxy/../)');
}

const staticPath = path.resolve(__dirname, '..', relativePath);
if (!staticPath) {
  throw new Error(`process.env.RELATIVE_PATH does not exist (${staticPath})`);
}

const key = fs.readFileSync(path.resolve(__dirname, '..', relativePath, 'key.pem'), 'utf8');
const cert = fs.readFileSync(path.resolve(__dirname, '..', relativePath, 'cert.pem'), 'utf8');
const opts = { key, cert };

var proxy = httpProxy.createProxyServer({
  target: 'https://api.intellimize.co',
  ssl: opts,
  secure: true,
});

proxy.on('proxyRes', function(proxyRes, req, res) {
	res.setHeader('Access-Control-Allow-Origin', '*');
});

proxy.listen(8000);

const app = express();
app.use(express.static(path.resolve(__dirname, '..', relativePath)));

https.createServer(opts, app).listen(9000, () => {
  console.log('Server running on port 9000');
});
