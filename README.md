# simple http proxy server #

## Setup

1. Install dependencies

```
npm i
```

2. Put this folder as a sibling of your project

```
- proxy
- myproject
```

3. Add the following to your `/etc/hosts` file

```
127.0.0.1 www.api.intellimize.co
```

4. If you have not already, generate an unsigned SSL key/cert in your project directory:

```
cd myproject
openssl req -newkey rsa:2048 -new -nodes -x509 -days 3650 -keyout key.pem -out cert.pem
cd ..
```

5. change directories into `proxy`

```
cd proxy
```

6. edit `.env` to include the relative path to your project from the _parent_ of the proxy folder

```
RELATIVE_PATH=myproject
```

7. run `npm start` from within the proxy folder

8. launch chrome with the `--ignore-certificate-errors` since we are using a self-signed cert that won't match the domain authority

```
/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --ignore-certificate-errors &> /dev/null &
```

8. open `https://localhost:9000/` in a browser
